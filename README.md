This is a simple RESTful project with MySQL's example World DB. It uses Spring Boot and Hibernate.

If you want to use it, you have to run world.sql from MySQL server on your computer. 

Also you have to change application.properties root id and password.
