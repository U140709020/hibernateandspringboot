package com.javacourse.project.hibernateandspringboot.repository;

import java.util.List;

import com.javacourse.project.hibernateandspringboot.model.City;

public interface ICityDal {
	List<City> GetAll();
	void add(City city);
	void update(City city);
	void delete(City city);
	City getById(int id);
}
