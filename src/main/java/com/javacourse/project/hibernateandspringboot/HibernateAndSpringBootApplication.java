package com.javacourse.project.hibernateandspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HibernateAndSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(HibernateAndSpringBootApplication.class, args);
	}

}
